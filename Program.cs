﻿using CampaingQueryMonitor;
using CampaingQueryMonitor.Models.Settings;
using CampaingQueryMonitor.Providers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;

namespace CampaignQueueMonitor
{
    internal static class Task
    {
        public static async System.Threading.Tasks.Task Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables()
            .Build();

            await Host.CreateDefaultBuilder(args)
            .ConfigureServices(services =>
            {
                services.AddHttpClient();
                services.AddScoped<ZendeskProvider>();
                services.AddScoped<ServersProvider>();

                // Add configurations
                services.Configure<CampaignSettings>(options => config.GetSection("Campaign").Bind(options));
                services.Configure<VisualiserSettings>(options => config.GetSection("Visualiser").Bind(options));
                services.Configure<ZendeskSettings>(options => config.GetSection("Zendesk").Bind(options));

                // Start point for run sync
                services.AddHostedService<SendDataProcessor>();                
            })
            .Build()
            .RunAsync();
        }

    }
}