﻿
namespace CampaingQueryMonitor
{
    public static class Constants
    {
        public static class MetricsNames
        {
            public const string Zendesk = "Zendesk.Metric";
            public const string Campaign = "Campaign.";
        }

        public const string ValueFailString = "failed";
    }
}
