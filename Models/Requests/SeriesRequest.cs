﻿
using System.Collections.Generic;

namespace CampaingQueryMonitor.Models.Requests
{
    public class SeriesRequest
    {
        public List<MetricModel> Metric { get; set; }

        public void AppendMetric(MetricModel metric)
        {
            Metric = Metric ?? new List<MetricModel>();
            Metric.Add(metric);
        }
    }
}
