﻿
using System.Collections.Generic;

namespace CampaingQueryMonitor.Models
{
    public class MetricModel
    {
        public string Metric { get; set; }
        // Timestamp, value
        public List<List<string>> Points { get; set; }
        public string Type { get; set; }
    }
}
