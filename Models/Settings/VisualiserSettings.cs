﻿
namespace CampaingQueryMonitor.Models.Settings
{
    public class VisualiserSettings
    {
        public string SeriesUri { get; set; }
        public string VisualiserApiKey { get; set; }
    }
}
