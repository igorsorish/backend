﻿
namespace CampaingQueryMonitor.Models.Settings
{
    public class ZendeskSettings
    {
        public string QueueCountUrl { get; set; }
        public string AuthToken { get; set; }
    }
}
