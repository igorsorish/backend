﻿
namespace CampaingQueryMonitor.Models.Settings
{
    public class CampaignSettings
    {
        public string Protocol { get; set; }
        public string MainDomain { get; set; }
        public string[] ServersSubdomains { get; set; }
    }
}
