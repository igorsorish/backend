﻿using CampaingQueryMonitor.Models;
using CampaingQueryMonitor.Models.Settings;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CampaingQueryMonitor.Providers
{
    public class ZendeskProvider: ServersProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IOptions<ZendeskSettings> _zendeskSettings;

        public ZendeskProvider(IHttpClientFactory httpClientFactory,
            IOptions<ZendeskSettings> zendeskSettings
            ): base(httpClientFactory)
        {
            _zendeskSettings = zendeskSettings;
        }

        public override async Task<string> GetValueCount(string url)
        {
            string count;

            try
            {
                // Get response
                HttpClient httpClient = _httpClientFactory.CreateClient();
                httpClient.DefaultRequestHeaders.Add("Authorization", _zendeskSettings.Value.AuthToken);
                HttpResponseMessage response = await httpClient.GetAsync(url);

                // Get content of page
                HttpContent content = response.Content;
                string jsonContent = await content.ReadAsStringAsync();

                // Get count
                count = JObject.Parse(jsonContent)["count"].ToString();

            }
            catch (Exception)
            {
                count = Constants.ValueFailString;
            }

            return count;
        }
    }
}
