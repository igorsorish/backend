﻿using CampaingQueryMonitor.Models;
using CampaingQueryMonitor.Models.Settings;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CampaingQueryMonitor.Providers
{
    public class ServersProvider: IFetchProvider
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public string Type { get; set; } = "count";

        public ServersProvider(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<MetricModel> FetchData(string url, long timestamp, string metricName)
        {
            var count = await GetValueCount(url);

            // Prepare response
            var result = new MetricModel
                {
                    Metric = metricName,
                    Points = new List<List<string>> { new List<string> { timestamp.ToString(), count } },
                    Type = Type
                };
             return result;
        }

        public virtual async Task<string> GetValueCount(string url)
        {
            string count;

            try
            {
                // Get response
                HttpClient httpClient = _httpClientFactory.CreateClient();
                HttpResponseMessage response = await httpClient.GetAsync(url);

                // Get content of page
                HttpContent content = response.Content;

                // To string for parse data
                string htmlContent = await content.ReadAsStringAsync();
                const string newCount = "new count: (.*)";
                var match = new Regex(newCount, RegexOptions.IgnoreCase).Match(htmlContent);

                // Get count
                count = match.Groups[1].Value;

            }
            catch (Exception)
            {
                count = Constants.ValueFailString;
            }

            return count;
        }
    }
}
