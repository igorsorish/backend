﻿using CampaingQueryMonitor.Models;
using System.Threading.Tasks;

namespace CampaingQueryMonitor.Providers
{
    public interface IFetchProvider
    {
        string Type { get; set; }

        Task<MetricModel> FetchData(string url, long timestamp, string metricName);
    }
}
