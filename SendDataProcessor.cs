﻿using CampaingQueryMonitor.Models;
using CampaingQueryMonitor.Models.Requests;
using CampaingQueryMonitor.Models.Settings;
using CampaingQueryMonitor.Providers;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CampaingQueryMonitor
{
    public class SendDataProcessor: IHostedService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ServersProvider _servers;
        private readonly ZendeskProvider _zendsesk;
        private readonly IOptions<CampaignSettings> _campaignSettings;
        private readonly IOptions<ZendeskSettings> _zendeskSettings;
        private readonly IOptions<VisualiserSettings> _visualiserSettings;


        public SendDataProcessor(
            IHttpClientFactory httpClientFactory, 
            ServersProvider servers, 
            ZendeskProvider zendsesk,
            IOptions<CampaignSettings> campaignSettings,
            IOptions<ZendeskSettings> zendeskSettings,
            IOptions<VisualiserSettings> visualiserSettings)
        {
            _httpClientFactory = httpClientFactory;
            _servers = servers;
            _zendsesk = zendsesk;
            _campaignSettings = campaignSettings;
            _zendeskSettings = zendeskSettings;
            _visualiserSettings = visualiserSettings;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            // Get timestamp
            var epochTimestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            
            // ToDo uncomment if support one object send with multiple metrics
            // Prepare series model
            //var dataObject = new SeriesRequest();
            //dataObject.Metric = new List<MetricModel>();

            // List of task for make parallel
            var requests = new List<Task<MetricModel>>();

            // Fetch data tasks for Servers by subdomain list
            foreach (var subdomain in _campaignSettings.Value.ServersSubdomains)
            {
                requests.Add(
                    _servers.FetchData(
                        $"{_campaignSettings.Value.Protocol}://{subdomain}.{_campaignSettings.Value.MainDomain}", 
                        epochTimestamp,
                        Constants.MetricsNames.Campaign + subdomain
                        ));
            }

            // Fetch data task from zendesk service
            requests.Add(
                _zendsesk.FetchData(
                    _zendeskSettings.Value.QueueCountUrl, 
                    epochTimestamp,
                    Constants.MetricsNames.Zendesk
                    )
                );

            Task.WaitAll(requests.ToArray());

            
            foreach (var request in requests)
            {
                var dataObject = new SeriesRequest();
                dataObject.AppendMetric(request?.Result);
                await SendData(dataObject);
            }

            // ToDo uncomment if support one object send with multiple metrics
            //await SendData(dataObject);
        }

        public async Task SendData(SeriesRequest dataObject)
        {
            // Create httpClient and post data to endpoint
            HttpClient httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri(_visualiserSettings.Value.SeriesUri);

            var json = JsonConvert.SerializeObject(dataObject);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            try
            {
                await httpClient.PostAsync(
                    $"?api_key={_visualiserSettings.Value.VisualiserApiKey}",
                    data
                );
            }
            catch (Exception)
            {
                throw new InvalidOperationException();
            }

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
